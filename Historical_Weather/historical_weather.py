#!/usr/bin/env python3

func = None
city = None
year = None
month = None

def main():
    """command to produce the greatest daily temperature
    change for a particular city (max-temp-delta), and
    the average yearly days of precipitation for
    a particular city (days-of-precip) from a csv containing
    the relevant data"""

    import sys

    get_inputs()
    
    if func == "daysofprecip":
        print(days_of_precip(city))
    elif func == "maxtempdelta":
        print(max_temp_delta(city, year, month))
    else:
        sys.exit("Invalid function")

def get_inputs():
    """collects the function, city, and dates to be processed
    while also determining which options are included and
    if they are valid"""
    # this should have been argparse

    import sys
    global func
    global city
    global year
    global month

    if len(sys.argv) < 3:
        sys.exit("Not enough arguments")

    if sys.argv[1] == "days-of-precip":
        if len(sys.argv) > 3:
            sys.exit("Too many arguments")

        func = "daysofprecip"

        if sys.argv[2].isalpha():
            city = sys.argv[2].upper()
        else:
            sys.exit("Incorrect value for City")


    elif sys.argv[1] == "max-temp-delta":
        func = "maxtempdelta"

        if len(sys.argv) == 3:
            if sys.argv[2].isalpha():
                city = sys.argv[2].upper()
            else:
                sys.exit("Invalid Input")

        if len(sys.argv) >= 4:
            if sys.argv[2].isalpha() and city == None:
                city = sys.argv[2].upper()
            elif len(str(sys.argv[2])) == 4 and (2010 <= int(sys.argv[2]) <= 2019) and year == None:
                year = int(sys.argv[2])
            elif len(str(sys.argv[2])) <= 2 and (0 < int(sys.argv[2]) <= 12) and month == None:
                if len(sys.argv) == 5:
                    month = int(sys.argv[2])
                else:
                    sys.exit("Specifying a month requires also a year")
            else:
                sys.exit("Invalid Input")

            if sys.argv[3].isalpha() and city == None:
                city = sys.argv[3].upper()
            elif len(str(sys.argv[3])) == 4 and (2010 <= int(sys.argv[3]) <= 2019) and year == None:
                year = int(sys.argv[3])
            elif len(str(sys.argv[3])) <= 2 and (0 < int(sys.argv[3]) <= 12) and month == None:
                if len(sys.argv) == 5:
                    month = int(sys.argv[3])
                else:
                    sys.exit("Specifying a month requires also a year")
            else:
                sys.exit("Invalid Input")

            if len(sys.argv) == 5:
                if sys.argv[4].isalpha() and city == None:
                    city = sys.argv[4].upper()
                elif len(str(sys.argv[4])) == 4 and (2010 <= int(sys.argv[4]) <= 2019) and year == None:
                    year = int(sys.argv[4])
                elif len(str(sys.argv[4])) <= 2 and (0 < int(sys.argv[4]) <= 12) and month == None:
                    month = int(sys.argv[4])
                else:
                    sys.exit("Invalid Input")


    else:
        sys.exit("Invalid function")






def days_of_precip(city):
    """Calculates the average number of days per
    year the given city had non-zero precipitation
    (either snow or rain) based on the entire 10 year period"""
    import pandas as pd
    import numpy as np
    import json

    # read the csv into pandas as a dataframe
    df = pd.read_csv(r'./noaa_historical_weather_10yr.csv', parse_dates = True, usecols=['NAME','DATE','PRCP','SNOW'])[['NAME','DATE','PRCP','SNOW']]
    # create year column
    df['YEAR'] = pd.to_datetime(df['DATE']).dt.year
    # replace any non-values with zero so that math works
    df = df.replace(np.NAN, 0)
    # add snow and precipitation as one value in new column
    df['PRECIP'] = df['PRCP'] + df['SNOW']
    # replace zeros with non-numbers for easy counting
    df = df.replace(0, np.NAN)
    # set the name of the city to what is in the data for consistant output
    full_city = df[df['NAME'].str.contains(city)].iloc[0,0]
    # count the rows with a value for precipitation, group the years by a count of how many times it appears for the given city, then count the years; divide the days of precipitation by the number of years
    days_of_precip = df[df['NAME'].str.contains(city)]['PRECIP'].count()/(df[df['NAME'].str.contains(city)].groupby(['YEAR'])['YEAR'].count()).count()
    # format result and city name as json
    data = { full_city: { "days_of_precip": days_of_precip } }

    result = json.dumps(data)
    return(result)

def max_temp_delta(city, year, month):
    """Determines the greatest single day low to high
    temperature change for the designated city and
    time period (all time, yearly, monthly)."""

    import pandas as pd

    # read the csv into pandas as a dataframe
    df = pd.read_csv(r'./noaa_historical_weather_10yr.csv', parse_dates = True, usecols=['NAME','DATE','TMAX','TMIN'])[['NAME','DATE','TMAX','TMIN']]

    # format the date column as datetime and extract the year, month, and day to new columns
    df['DATE'] = pd.to_datetime(df['DATE'])
    df['YEAR'] = df['DATE'].dt.year
    df['MONTH'] = df['DATE'].dt.month
    df['DAY'] = df['DATE'].dt.day

    # calculate the temperature delta for each day in a new column
    df['temp_change'] = df['TMAX'] - df['TMIN']

    # get the row with the largest delta filtered by the specified options
    if month == None:
        if year == None:
            max_delta = df.iloc[df[df['NAME'].str.contains(city)].groupby(['NAME'])['temp_change'].idxmax()]
        else:
            max_delta = df.iloc[df[(df['NAME'].str.contains(city)) & (df['YEAR'] == year)].groupby(['NAME', 'YEAR'])['temp_change'].idxmax()]
    else:
        max_delta = df.iloc[df[(df['NAME'].str.contains(city)) & (df['YEAR'] == year) & (df['MONTH'] == month)].groupby(['NAME', 'YEAR', 'MONTH'])['temp_change'].idxmax()]

    # filter the row from the previous step to only have the name, date, and temperature delta
    max_delta = max_delta[['NAME','DATE','temp_change']]
    # setting the index to the name makes it the top-level of the json output
    max_delta = max_delta.set_index('NAME')
    # convert the dataframe to json
    result = max_delta.to_json(orient="index")
    return(result)

if __name__ == '__main__':
    main()
