# -*- coding: utf-8 -*-
import ssl
orig_sslsocket_init = ssl.SSLSocket.__init__
ssl.SSLSocket.__init__ = lambda *args, cert_reqs=ssl.CERT_NONE, **kwargs: orig_sslsocket_init(*args, cert_reqs=ssl.CERT_NONE, **kwargs)

from urllib.request import urlopen
import requests
import pandas as pd
#import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.dates as mdates
import altair as alt
from vega_datasets import data
import plotly.express as px
import time

t = time.localtime()
timestamp = time.strftime('%b-%d-%Y_%H%M%S', t)

ptc15_url="https://data.cdc.gov/resource/2ew6-ywp6.json?wwtp_jurisdiction=New%20York&county_names=Monroe"
concentration_url="https://data.cdc.gov/resource/g653-rqe2.json?$query=SELECT%20%60key_plot_id%60%2C%20%60date%60%2C%20%60pcr_conc_smoothed%60%2C%20%60normalization%60%0AWHERE%0A%20%20%60key_plot_id%60%20IN%20%28%0A%20%20%20%20%27NWSS_ny_1003_Treatment%20plant_raw%20wastewater%27%2C%0A%20%20%20%20%27NWSS_ny_1010_Treatment%20plant_raw%20wastewater%27%2C%0A%20%20%20%20%27NWSS_ny_580_Treatment%20plant_raw%20wastewater%27%0A%20%20%29"
location_url="https://covid.cdc.gov/covid-data-tracker/COVIDData/getAjaxData?id=hhs_nwss_cdt_latest"
response = urlopen(ptc15_url)
d = requests.get(ptc15_url)
df = pd.DataFrame(pd.read_json(response))


response2 = urlopen(concentration_url)
d2 = requests.get(concentration_url)
df2 = pd.DataFrame(pd.read_json(response2))
discard = ["LUMINULTRA"]
df = df[~df.key_plot_id.str.contains('|'.join(discard))]

response3 = urlopen(location_url)
d3 = requests.get(location_url)
df_loc = pd.DataFrame(pd.json_normalize(pd.read_json(response3)["HHS_NWSS_CDT_Latest"]))
df_loc = df_loc.dropna(subset=['percentile', 'ptc_15d'])
df_loc = df_loc[df_loc.percentile != 999.00]

#df_loc = df_loc[['latitude', 'longitude', 'wwtp_jurisdiction', 'wwtp_id', 'county_names', 'county_fips', 'key_plot_id', 'population_served']]
avcpct = df_loc.groupby("county_fips")["percentile"].mean()
states = alt.topo_feature(data.us_10m.url, feature='states')
# US states background
background = alt.Chart(states).mark_geoshape(
    fill='lightgray',
    stroke='white'
).properties(
    width=500,
    height=300
).project('albersUsa')

points = alt.Chart(df_loc).mark_circle().encode(
    longitude = 'longitude:Q',
    latitude = 'latitude:Q',
    size = alt.Size('population_served:Q', title='population_served'),
    color = alt.value('steelblue'),
    tooltip = ['wwtp_id:N','county_names:N','population_served:Q']
).properties(
    title='wastewater test locations'
)
location_map = background + points
location_map.save('wwtp_map2.html')

df['date_start'] = pd.to_datetime(df['date_start'])


df['date_end'] = pd.to_datetime(df['date_end'])
df = df.sort_values(by='date_end')
df['date'] = df['date_end']
df['wwtp_id'] = df['wwtp_id'].apply(str)
chart =alt.Chart(df).mark_line().encode(
    alt.X('date_end:T', title = '15-day end date', axis=alt.Axis(format='%x')),
    alt.Y('percentile:Q', title = 'percentile'),
    alt.Color('wwtp_id:N', legend = alt.Legend(title = 'Collection Site'))
)
chart.save('all_percentile3.html')
df2['date'] = pd.to_datetime(df2['date'])
df2 = df2.sort_values(by='date')


df3 = df
df3 = df3.dropna(subset=['percentile'])
df3 = df3.set_index(df3['date'])
for pid in set(df3['wwtp_id']):
    filtered_df3 = df3[df3['wwtp_id'] == pid]
    df3[pid] = filtered_df3['percentile']

trimmed_df3 = df3.drop_duplicates()

chart = alt.Chart(trimmed_df3).mark_line().encode(
    alt.X('date_end', title = '15-day end date'),
    alt.Y('580:Q', title = 'percentile')
    )
chart.save('percentile.html')
plt.figure()
trimmed_df3.plot()
plt.show()

fig = px.line(df3, x=df3.date, y=df3.percentile, color=df3.wwtp_id)
fig.update_xaxes(type="date")
fig.update_yaxes(type="-")
fig.write_html(timestamp + 'percentile.html')

df4 = df
df4 = df4.dropna(subset=['ptc_15d'])

df4 = df4.set_index(df4['date'])
for pid in set(df4['wwtp_id']):
    filtered_df4 = df4[df4['wwtp_id'] == pid]
    df4[pid] = filtered_df4['ptc_15d']
    

cols_to_keep2 = list(set(df4['wwtp_id']))
trimmed_df4 = df4[cols_to_keep2]
trimmed_df4 = trimmed_df4.drop_duplicates()

plt.figure()
trimmed_df4.plot()
plt.show()
fig = px.line(df3, x=df3.date, y=df3.ptc_15d, color=df3.wwtp_id)
fig.update_xaxes(type="date")
fig.update_yaxes(type="-")
fig.write_html(timestamp + 'ptc15d.html')

df5 = df2.set_index(df2['date'])
for pid in set(df5['key_plot_id']):
    filtered_df5 = df5[df5['key_plot_id'] == pid]
    df5[pid] = filtered_df5['pcr_conc_smoothed']
    
cols_to_keep3 = list(set(df5['key_plot_id']))
trimmed_df5 = df5[cols_to_keep3]
trimmed_df5 = trimmed_df5.drop_duplicates()
trimmed_df5 = trimmed_df5.rename({'NWSS_ny_580_Treatment plant_raw wastewater': 'East - 580', 'NWSS_ny_1010_Treatment plant_raw wastewater': 'West - 1010', 'NWSS_ny_1003_Treatment plant_raw wastewater': 'City - 1003'}, axis=1)

plt.figure()
trimmed_df5.plot()
plt.show()
fig = px.line(df5, x=df5.date, y=df5.pcr_conc_smoothed, color=df5.key_plot_id)
fig.update_xaxes(type="date")
fig.update_yaxes(type="-")
fig.write_html(timestamp + 'concentration.html')